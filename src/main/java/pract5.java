import deck.DeckOfCards;
import deck.DeckType;

import java.util.Scanner;

/**
 * A program that emulates de game of seven and a half in your computer.
 * You play against it.
 *
 * @author Arnau Mora Gras
 * @version 2021/01/24
 */
public class pract5 {

    /**
     * Gives an alias for printing to System.out
     *
     * @param msg The content to print
     * @see System
     */
    public static void p(String msg) {
        System.out.print(msg);
    }

    /**
     * Gives an alias for printing to System.out
     *
     * @param line The line to print
     * @see System
     */
    public static void pln(String line) {
        System.out.println(line);
    }

    public static void main(String[] args) {
        pln("=== BIENVENIDO AL JUEGO DEL 7 Y MEDIO ===");
        pln("Las reglas son simples:");
        pln("1. Jugarás contra la máquina, tu empiezas, guay.");
        pln("2. Deberás ir cogiendo cartas de la baraja española, cada una tiene");
        pln("   un valor de puntos igual a su número, excepto las figuras, que");
        pln("   valen medio punto.");
        pln("3. Debes estar lo más cerca posible de 7.5 puntos sin pasarte.");
        pln("");
        pln("Ánimo, esto empieza ya, te doy tu primera carta:");

        Scanner input = new Scanner(System.in);
        while (true) {
            DeckOfCards human = new DeckOfCards(DeckType.FULL);
            DeckOfCards computer = new DeckOfCards(DeckType.FULL);

            boolean retired = false;
            do {
                SieteYMedio.playHuman(human);

                pln("TU MANO:");
                human.show();
                pln("Quieres seguir jugando? [S/n]");
                String answ = input.nextLine();
                if (answ.equalsIgnoreCase("n"))
                    retired = true;
            } while (!retired);

            pln("");

            float playerPunctuation = human.points();
            if (playerPunctuation <= 7.5f) {
                pln("Muy bien, tienes una puntuación de %f puntos. Veremos que tal".formatted(playerPunctuation));
                pln("le va a tu rival....");
                pln("");
                p("*Juega la máquina...");

                SieteYMedio.playAuto(computer, playerPunctuation);
                float machinePunctuation = computer.points();

                pln("fin");
                pln("La máquina ha obtenido %f puntos.".formatted(machinePunctuation));
                pln("");

                if (machinePunctuation <= 7.5f)
                    if (machinePunctuation > playerPunctuation)
                        // The machine has won
                        pln("Gana la máquina, lo siento!");
                    else if (playerPunctuation == machinePunctuation)
                        pln("Empate técnico! Los dos teneis la misma puntuación.");
                    else
                        pln("Has ganado!! Toma una piruleta :)");
                else
                    pln("Has ganado!! Toma una piruleta :)");
            } else
                pln("Te has pasado de 7.5 puntos! Gana la máquina, lo siento!");

            pln("");
            pln("Quieres volver a jugar? [S/n]");
            String answ = input.nextLine();
            if (answ.equalsIgnoreCase("n"))
                break;
        }
        input.close();
    }
}
