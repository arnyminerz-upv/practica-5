package exceptions;

public class CardsDeckEmptyException extends Exception{
    public CardsDeckEmptyException(){
        super("The cards deck doesn't contain any card");
    }
}
