import deck.DeckOfCards;
import exceptions.CardsDeckEmptyException;

/**
 * This class contains all the game logic for moving the player's wants, and the machine AI.
 * @author Arnau Mora
 * @version 2021/01/24
 * @see DeckOfCards
 */
public class SieteYMedio {
    /**
     * Gives the human a new card.
     * @param deck The deck of the human
     */
    public static void playHuman (DeckOfCards deck) {
        try {
            deck.takeCard();
        } catch (CardsDeckEmptyException e) {
            e.printStackTrace();
        }
    }

    public static void playAuto (DeckOfCards deck, float playerPoints) {
        while( deck.points() < playerPoints ) {
            try {
                deck.takeCard();
            } catch (CardsDeckEmptyException e) {
                e.printStackTrace();
            }
        }
    }
}