package deck;

import cards.Card;
import cards.CardSuit;
import exceptions.CardsDeckEmptyException;
import utils.Utils;

import java.util.ArrayList;
import java.util.Collections;

/**
 * A deck of the cards class
 * @author Arnau Mora
 * @see Card
 * @see DeckType
 */
public class DeckOfCards {
    /**
     * The list of cards the deck has
     */
    private final ArrayList<Card> cards = new ArrayList<>();
    private boolean[] usedCards = new boolean[0];

    /**
     * Checks if a card has already been given
     * @param index The index to check
     * @return If the card has already been given
     * @throws CardsDeckEmptyException If the cards deck is empty
     */
    private boolean hasCardBeenGiven(int index) throws CardsDeckEmptyException {
        if (isEmpty())
            throw new CardsDeckEmptyException();
        return usedCards[index];
    }

    public int randomCard() throws CardsDeckEmptyException {
        if (isEmpty())
            throw new CardsDeckEmptyException();
        return Utils.getRandomNumber(0, size());
    }

    public DeckOfCards(DeckType type){
        if (type == DeckType.FULL) {
            // Generate the 40 cards for the deck
            for (CardSuit suit : CardSuit.values())
                for (int r = 0; r < Card.ranksCount(); r++)
                    add(new Card(r, suit));
        }
    }

    /**
     * Adds a card to the deck.
     * Note that this will reset the used cards.
     * @param card The card to add
     */
    public void add(Card card){
        cards.add(card);
        usedCards = new boolean[size()];
    }

    /**
     * Empties the deck
     */
    public void clear(){
        cards.clear();
    }

    /**
     * Mixes all the cards.
     * This also returns all the used cards to the deck.
     */
    public void shuffle(){
        Collections.shuffle(cards);
        usedCards = new boolean[size()];
    }

    /**
     * Returns an specific card from the deck
     * @param index The index of the card
     * @return A card at the index {index} of the deck.
     */
    public Card get(int index){
        return cards.get(index);
    }

    /**
     * Returns the size of the deck.
     * @return The size of the deck
     */
    public int size(){
        return cards.size();
    }

    /**
     * Checks if the deck of cards is empty.
     * @return True if any card has been added yet. False otherwise
     */
    public boolean isEmpty(){
        return size() <= 0;
    }

    /**
     * Gives the user a random card that has not already been given.
     * @return A random card
     * @throws CardsDeckEmptyException If the deck is empty
     */
    public Card takeCard() throws CardsDeckEmptyException {
        int card;
        do {
            card = randomCard();
        }while (hasCardBeenGiven(card));

        usedCards[card] = true;

        return get(card);
    }

    /**
     * Returns the amount of points the deck has
     * @return The amount of points the deck has
     */
    public float points(){
        float points = 0;
        for(int c = 0; c < usedCards.length; c++)
            if (usedCards[c])
                points += get(c).getPoints();

        return points;
    }

    /**
     * Shows through System.out the cards the deck has
     */
    public void show(){
        int u = 0;
        for (int c = 0; c < cards.size(); c++)
            if (usedCards[c]) {
                System.out.print(get(c) + " ");

                // Print a line jump every 10 lines
                u++;
                if (u % 10 == 0) System.out.println();
            }
        System.out.println();
    }
}
