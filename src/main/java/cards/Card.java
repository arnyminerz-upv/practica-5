package cards;

/**
 * A game card
 * @author Arnau Mora
 * @see CardSuit
 */
public class Card {
    private static final String[] rankNames = {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "Sota",
            "Caballo",
            "Rey",
    };
    private static final float[] rankPoints = {
            1, 2, 3, 4, 5, 6, 7, 0.5f, 0.5f, 0.5f
    };

    /**
     * Gets the amount of ranks available
     * @return The amount of ranks available
     */
    public static int ranksCount(){
        return rankNames.length;
    }

    private final int rank;
    private final CardSuit suit;

    /**
     * The card class constructor. Builds a new card
     * @param rank The rank of the card
     * @param suit The suit of the card
     */
    public Card(int rank, CardSuit suit){
        this.rank = rank;
        this.suit = suit;
    }

    /**
     * Returns the rank of the card
     * @return The rank of the card
     */
    public int getRank() {
        return rank;
    }

    /**
     * Returns the suit of the card
     * @return The suit of the card
     */
    public CardSuit getSuit() {
        return suit;
    }

    /**
     * Returns the amount of points the card gives
     * @return The amount of points the card gives
     */
    public float getPoints(){
        return rankPoints[rank];
    }

    /**
     * Transforms the card into an user-readable string
     * @return Gets an user-readable string
     */
    @Override
    public String toString() {
        return "%s de %s".formatted(rankNames[rank], suit.displayName);
    }
}
