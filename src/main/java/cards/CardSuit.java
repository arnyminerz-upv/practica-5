package cards;

public enum CardSuit {
    OROS("Oros"), ESPADAS("Espadas"), BASTOS("Bastos"), COPAS("Copas");

    /**
     * The name that will be displayed to the user
     */
    // getDisplayName is not generated since displayName is final, so modification is not possible.
    public final String displayName;

    /**
     * Defines a card suit
     * @param displayName The name that will be displayed to the user
     */
    CardSuit(String displayName){
        this.displayName = displayName;
    }
}
